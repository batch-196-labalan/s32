let http = require("http");


http.createServer(function(request,response){

    console.log(request.url);
    console.log(request.method);

    if(request.url === "/"){

        response.writeHead(200,{'Content-Type':'text/plain'});
        response.end("Welcome to Booking System");

    } else if (request.url === "/profile" && request.method === "GET"){

        response.writeHead(200,{'Content-Type':'text/plain'});
        response.end("Welcome to your Profile!");

    } else if (request.url === "/courses" && request.method === "GET"){

        response.writeHead(200,{'Content-Type':'text/plain'});
        response.end("Here's our updated courses");

    } else if (request.url === "/addcourse" && request.method === "POST"){

        response.writeHead(200,{'Content-Type':'text/plain'});
        response.end("Add course to our resources");

    } else if (request.url === "/updateourse" && request.method === "PUT"){

        response.writeHead(200,{'Content-Type':'text/plain'});
        response.end("Update a course to our resources");

    } else if (request.url === "/archivecourse" && request.method === "DELETE"){

        response.writeHead(200,{'Content-Type':'text/plain'});
        response.end("Archive a course to our resources");
       
    }

}).listen(4000);

console.log("Server running at localhost:4000");







