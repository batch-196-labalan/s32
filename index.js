let http = require("http");

let courses = [

    {
        name: "Python 101",
        description: "Learn Python",
        price: 25000
    },
    {
        name: "ReactJS 101",
        description: "Learn React",
        price: 35000
    },
    {
        name: "ExpressJS 101",
        description: "Learn ExpressJS",
        price: 28000
    }

];


http.createServer(function(request,response){

    console.log(request.url);
    console.log(request.method);

    if(request.url === "/" && request.method === "GET"){

        response.writeHead(200,{'Content-Type':'text/plain'});
        response.end("Hello from our 4th server. GET method request.");

    } else if (request.url === "/" && request.method === "POST"){

        response.writeHead(200,{'Content-Type':'text/plain'});
        response.end("This is the / endpoint. POST method request.");

    } else if (request.url === "/" && request.method === "PUT"){

        response.writeHead(200,{'Content-Type':'text/plain'});
        response.end("This is the / endpoint. PUT method request.");

    } else if (request.url === "/" && request.method === "DELETE"){

        response.writeHead(200,{'Content-Type':'text/plain'});
        response.end("This is the / endpoint. DELETE method request.");

    } else if (request.url === "/courses" && request.method === "GET"){

        response.writeHead(200,{'Content-Type':'application/json'});
        //response.end("This is a response to a GET method request for the /courses endpoint.");
        response.end(JSON.stringify(courses));

    } else if (request.url === "/courses" && request.method === "POST"){


    	/* 2 steps in receiving data from the request in Node Js

	  let requestBody = " "; placeholder to contain the data (request body) passed from the client.

    	1.DATA STEP - will read the incoming stream of data from the client and process it so we can save it in the request body variablle


    	*/

    	let requestBody = " ";

    	request.on('data', function(data){

    		//console.log(data); //stream of data from the client

    		requestBody += data; //data stream is saved into the variable as a string

    	})
  
  		//end strep - this will run once or after the request data has been completely sent from the client

  		request.on('end',function(){

  				//completely received the data
  			console.log(requestBody);


  		//Initially, requestBody is in Json format we cannot add this into our courses array because it's a string so we have to update requestBody variable with a parsed version of the received JSON format
  		requestBody = JSON.parse(requestBody);

  		// console.log(requestBody);

  		courses.push(requestBody);

  		//check the courses array if the course was added
  		// console.log(courses);

  		response.writeHead(200,{'Content-Type':'application/json'});
        response.end(JSON.stringify(courses));

  		})


       
    }

}).listen(4000);

console.log("Server running at localhost:4000");







